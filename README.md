# NestJS TypeORM Swagger PostgreSQL
Пример API.

## Требования к системе

- NodeJS
- PostgreSQL

## Установка

```
git clone git clone https://selectah@bitbucket.org/selectah/nest-torm-swagger-pg.git
cd nest-torm-swagger-pg
npm install
```

## Конфигурация

Создать файл **.env** на основе примера **.env.sample**
```
cp .env.sample .env
```
Установить переменные окружения в файле **.env**

## Запуск

```
npm start
```

## API URL

http://<API_HOST>:<API_PORT>

## URL документации

http://<API_HOST>:<API_PORT>/docs
