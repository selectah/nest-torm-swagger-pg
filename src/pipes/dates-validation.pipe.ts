import {
    PipeTransform,
    Injectable,
    ArgumentMetadata,
    BadRequestException,
} from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import moment = require('moment');

@Injectable()
export class DatesValidationPipe implements PipeTransform<any> {
    transform(value: any, { metatype }: ArgumentMetadata) {
        const object = plainToClass(metatype, value);
        const errors = [];
        const currentTimestamp = moment().startOf('day').unix();
        const fromTimestamp = moment(object.from).unix();
        const toTimestamp = moment(object.to).unix();

        if(fromTimestamp < currentTimestamp){
            errors.push('Значение параметра "from" меньше текущей даты.')
        }
        if(toTimestamp < fromTimestamp){
            errors.push('Значение параметра "to" меньше чем "from".');
        }
        if (errors.length > 0) {
            throw new BadRequestException(errors, 'Ошибка валидации.');
        }
        return value;
    }
}
