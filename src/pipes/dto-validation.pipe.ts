import {
    PipeTransform,
    Injectable,
    ArgumentMetadata,
    BadRequestException, ValidationError,
} from '@nestjs/common';
import {
    validate,
} from 'class-validator';
import { plainToClass } from 'class-transformer';

@Injectable()
export class DtoValidationPipe implements PipeTransform<any> {
    async transform(value: any, { metatype }: ArgumentMetadata) {
        const object = plainToClass(metatype, value);
        const errors = await validate(object);
        if (errors.length > 0) {
            throw new BadRequestException(this.formatErrors(errors), 'Ошибка валидации.');
        }
        return value;
    }

    private formatErrors(errors: ValidationError[]) {
        return errors.map(error => {
            for(const key in error.constraints){
                return error.constraints[key];
            }
        });
    }
}
