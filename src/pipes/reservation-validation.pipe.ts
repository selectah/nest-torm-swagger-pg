import {
    PipeTransform,
    Injectable,
    ArgumentMetadata,
    NotAcceptableException,
} from '@nestjs/common';
import { CreateReservationDto } from '../modules/reservations/dto/create-reservation.dto';
import { ReservationsService } from '../modules/reservations/reservations.service';

@Injectable()
export class ReservationValidationPipe implements PipeTransform<CreateReservationDto> {
    constructor(private reservationsService: ReservationsService){

    }
    async transform(value: CreateReservationDto, { metatype }: ArgumentMetadata) {
        const reservation = await this.reservationsService.findByRoomIdAndDateRange(value);
        if (reservation) {
            throw new NotAcceptableException(
              `Комната №${reservation.room.number} забронирована c ${reservation.from} по ${reservation.to}.`,
              'Ошибка валидации.'
            );
        }
        return value;
    }
}
