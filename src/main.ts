import * as dotenv from 'dotenv';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './modules/app.module';
import { Seeder } from './modules/seeder/seeder';
import { Logger } from '@nestjs/common';
import { validateConfig } from './functions/validate-env-config';

async function bootstrap() {
  dotenv.config();
  validateConfig(process.env);
  const app = await NestFactory.create(AppModule);
  const logger = app.get(Logger);
  const seeder = app.get(Seeder);
  await seeder
    .seed()
    .then(() => {
      logger.debug('Инициализаци БД завершена успешно.');
    })
    .catch(error => {
      logger.error('Сбой инициализации БД.', error);
      throw error;
    });

  const swaggerConfig = new DocumentBuilder()
    .setTitle('Отель')
    .setDescription('API для отеля')
    .setVersion('1.0')
    .addTag('Отель')
    .build();
  const document = SwaggerModule.createDocument(app, swaggerConfig);
  SwaggerModule.setup('docs', app, document);

  await app.listen(process.env.API_PORT)
    .then(result => {
    logger.log(`Приложение готово принимать запросы по адресу http://${process.env.API_HOST}:${process.env.API_PORT}`)
    logger.log(`Спецификация по адресу http://${process.env.API_HOST}:${process.env.API_PORT}/docs`)
  })
    .catch(error => {
      logger.error('Сбой запуска приложения.', error);
    });
}

bootstrap();
