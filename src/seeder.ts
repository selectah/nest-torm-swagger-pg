import { Injectable, Logger } from '@nestjs/common';
import { RoomsService } from './modules/rooms/rooms.service';

@Injectable()
export class Seeder {
  constructor(
    private readonly logger: Logger,
    private readonly roomsService: RoomsService,
  ) {}

  async seed() {
    await this.populateRooms()
      .then(completed => {
        this.logger.log('Таблица "rooms" успешно заполнена.');
      })
      .catch(error => {
        this.logger.error('Ошибка при заполнении таблицы "rooms".');
      });
  }
  async populateRooms() {
    await this.roomsService.populate();
  }
}
