import { Logger, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Room } from './rooms/room.entity';
import { Reservation } from './reservations/reservation.entity';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { RoomsModule } from './rooms/rooms.module';
import { Seeder } from './seeder/seeder';
import { ReservationsModule } from './reservations/reservations.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        type: 'postgres',
        url: configService.get<string>('DATABASE_URL'),
        entities: [Room, Reservation],
        synchronize: true,
      }),
      inject: [ConfigService],
    }),
    RoomsModule,
    ReservationsModule
  ],
  providers: [Seeder, Logger],
  exports: [ConfigModule]
})
export class AppModule {
}
