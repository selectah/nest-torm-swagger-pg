import { RoomsService } from '../rooms/rooms.service';
import { Injectable, Logger } from '@nestjs/common';

@Injectable()
export class Seeder {
  constructor(
    private readonly logger: Logger,
    private readonly roomsService: RoomsService,
  ) {}

  async seed() {
    await this.rooms()
      .then(completed => {
        this.logger.log('Таблица "rooms" успешно заполнена.');
        Promise.resolve(completed);
      })
      .catch(error => {
        this.logger.error('Ошибка при заполнении таблицы "rooms".');
        Promise.reject(error);
      });
  }
  async rooms() {
    await this.roomsService.populate();
  }
}
