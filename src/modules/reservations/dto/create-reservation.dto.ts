import { OmitType } from '@nestjs/swagger';
import { ReservationDto } from './reservation.dto';

export class CreateReservationDto extends OmitType(ReservationDto, ['id']) {
}
