import { ApiProperty } from '@nestjs/swagger';
import { IsISO8601, IsNumber, IsPositive } from 'class-validator';

export class ReservationDto {

  @ApiProperty({ example: 1 })
  @IsPositive({ message: 'Значение параметра "id" меньше 0.' })
  @IsNumber({ maxDecimalPlaces: 0 }, { message: 'Значение параметра "roomId" должно быть целым числом.' })
  id: number;

  @ApiProperty({ example: 1 })
  @IsPositive({ message: 'Значение параметра "roomId" меньше 0.' })
  @IsNumber({ maxDecimalPlaces: 0 }, { message: 'Значение параметра "roomId" должно быть целым числом.' })
  roomId: number;

  @ApiProperty({ example: '2021-01-01' })
  @IsISO8601({}, { message: 'Некорректная дата "from"' })
  from: string;

  @ApiProperty({ example: '2021-01-01' })
  @IsISO8601({}, { message: 'Некорректная дата "to"' })
  to: string;

}
