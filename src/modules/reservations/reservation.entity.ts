import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Room } from '../rooms/room.entity';

export interface IReservation {
  id?: number;
  roomId: number;
  from: string;
  to: string;
}

@Entity({name: 'reservations'})
export class Reservation implements IReservation {
  @PrimaryGeneratedColumn('increment', {type: 'int8'})
  id?: number;

  @Column({ nullable: false, type: 'int2'})
  roomId: number;

  @Column({ nullable: false, type: 'date' })
  from: string;

  @Column({ nullable: false, type: 'date' })
  to: string;

  @ManyToOne(type => Room, room => room.id)
  room: Room;
}
