import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { LessThanOrEqual, MoreThanOrEqual, Repository } from 'typeorm';
import { Reservation } from './reservation.entity';
import { CreateReservationDto } from './dto/create-reservation.dto';

@Injectable()
export class ReservationsService {
  constructor(
    @InjectRepository(Reservation)
    private reservationsRepository: Repository<Reservation>,
  ) {
  }

  async create(reservationDto: CreateReservationDto): Promise<Reservation> {
    return await this.reservationsRepository.save(reservationDto);
  }

  async findByRoomIdAndDateRange(reservationDto: CreateReservationDto) {
    return await this.reservationsRepository.findOne({
      roomId: reservationDto.roomId,
      from: LessThanOrEqual( reservationDto.to),
      to: MoreThanOrEqual( reservationDto.from),
    }, {relations: ['room']});
  }
}
