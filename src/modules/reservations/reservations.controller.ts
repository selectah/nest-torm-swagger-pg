import { Body, Controller, Post, UsePipes, ValidationPipe } from '@nestjs/common';
import { ReservationsService } from './reservations.service';
import { CreateReservationDto } from './dto/create-reservation.dto';
import {
  ApiBadRequestResponse,
  ApiBody,
  ApiCreatedResponse,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { ReservationDto } from './dto/reservation.dto';
import { DatesValidationPipe } from '../../pipes/dates-validation.pipe';
import { ReservationValidationPipe } from '../../pipes/reservation-validation.pipe';
import { Reservation } from './reservation.entity';

@Controller('reservations')
@ApiTags('Бронирование комнат в отеле')
export class ReservationsController {
  constructor(private reservationsService: ReservationsService) {

  }

  @ApiOperation({
    summary: 'Бронирование номера.',
  })
  @ApiCreatedResponse({
    status: 201,
    description: 'Успешное создание бронирования.',
    type: ReservationDto,
  })
  @ApiBadRequestResponse({
    description: 'Проверка входных данных.',
    schema: {
      example: {
        statusCode: 400,
        message: [
          'Некорректный формат даты "from"',
        ],
        error: 'Ошибка валидации.',
      },
    },
  })
  @ApiResponse({
    status: 406,
    description: 'Комната забронирована.',
  })
  @ApiResponse({
    status: 500,
    description: 'Внутренняя ошибка сервера.',
  })
  @ApiBody({ type: CreateReservationDto })
  @UsePipes(ValidationPipe, DatesValidationPipe, ReservationValidationPipe)
  @Post()
  async createReservation(@Body() reservationDto: CreateReservationDto): Promise<Reservation> {
    return await this.reservationsService.create(reservationDto);
  }
}
