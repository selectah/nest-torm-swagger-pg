import { Column, Entity, PrimaryGeneratedColumn, Unique } from 'typeorm';

export interface IRoom {
  id?: number;
  number: number;
}

@Entity({ name: 'rooms' })
export class Room implements IRoom {

  @PrimaryGeneratedColumn('increment', { type: 'int2' })
  id?: number;

  @Column({ nullable: false, type: 'int2' })
  @Unique(['number'])
  number: number;

}
