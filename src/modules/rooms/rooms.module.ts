import { Logger, Module } from '@nestjs/common';
import { RoomsController } from './rooms.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Room } from './room.entity';
import { RoomsService } from './rooms.service';

@Module({
  imports: [TypeOrmModule.forFeature([Room])],
  providers: [RoomsService, Logger],
  controllers: [RoomsController],
  exports: [TypeOrmModule, RoomsService]
})
export class RoomsModule {}
