import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Room } from './room.entity';
import { getManager, Repository } from 'typeorm';

@Injectable()
export class RoomsService {
  constructor(
    @InjectRepository(Room)
    private roomsRepository: Repository<Room>,
  ) {
  }

  async populate() {
    const roomNumbers = Array.from({ length: parseInt(process.env.ROOMS_QUANTITY) }, (_, i) => i + 1);
    const insertNumbers: Room[] = roomNumbers.map(roomNumber => {
      return { number: roomNumber };
    });
    return await this.roomsRepository
      .find()
      .then(async rooms => {
        if (rooms.length) {
          return true;
        }
        return await this.roomsRepository.insert(insertNumbers);
      });
  }

  async findByDateRange(from: string, to: string): Promise<Room[]> {
    const reservedRoomsIdsQuery = getManager().createQueryBuilder()
      .distinct(true)
      .select('rsv."roomId"')
      .from('reservations', 'rsv')
      // TODO Выяснить как биндить параметры
      .where(`rsv.from < '${to}'`)
      .andWhere(`rsv.to > '${from}'`);

    const availableRoomsQuery = getManager().createQueryBuilder()
      .select('rm.id', 'id')
      .addSelect('rm.number', 'number')
      .from('rooms', 'rm')
      .where(`rm.id not in (${reservedRoomsIdsQuery.getQuery()})`)
      .orderBy('rm."number"');
    // console.log(availableRoomsQuery.getQuery());
    return await availableRoomsQuery.execute();
  }
}
