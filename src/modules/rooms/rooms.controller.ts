import { Controller, Get, Query } from '@nestjs/common';
import { RoomsService } from './rooms.service';
import { Room } from './room.entity';
import { ApiBadRequestResponse, ApiOkResponse, ApiOperation, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { RoomDto } from './dto/room.dto';
import { RoomsListRequestDto } from './dto/rooms-list-request.dto';
import { DtoValidationPipe } from '../../pipes/dto-validation.pipe';
import { DatesValidationPipe } from '../../pipes/dates-validation.pipe';

@Controller('rooms')
@ApiTags('Комнаты отеля')
export class RoomsController {
  constructor(private roomsService: RoomsService){

  }

  @ApiOperation({
    summary: 'Получение списка свободных комнат в диапазоне дат.',
  })
  @ApiQuery({
    name: 'from',
    type: 'string',
    required: true,
    example: '2021-05-25',
  })
  @ApiQuery({
    name: 'to',
    type: 'string',
    required: true,
    example: '2021-05-30',
  })
  @ApiOkResponse({
    type: [RoomDto],
  })
  @ApiBadRequestResponse({
    description: 'Проверка входных данных.',
    schema: {
      example: {
        statusCode: 400,
        message: [
          'Некорректный формат даты "to"',
          'Некорректный формат даты "from"'
        ],
        error: 'Ошибка валидации.'
      },
    },
  })
  @ApiResponse({
    status: 500,
    description: 'Внутренняя ошибка сервера.',
  })
  @Get()
  async getRooms(@Query(new DtoValidationPipe(), new DatesValidationPipe()) query: RoomsListRequestDto): Promise<Room[]> {
    return await this.roomsService.findByDateRange(query.from, query.to);
  }
}
