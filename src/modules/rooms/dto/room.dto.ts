import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsPositive } from 'class-validator';
import { IRoom } from '../room.entity';

export class RoomDto implements IRoom {
  @ApiProperty({ example: 1 })
  @IsPositive()
  @IsNumber()
  id: number;

  @ApiProperty({ example: 1 })
  @IsPositive()
  @IsNumber()
  number: number;
}
