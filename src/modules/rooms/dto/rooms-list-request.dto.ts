import { IsDefined, IsISO8601 } from 'class-validator';

export class RoomsListRequestDto {
  @IsDefined({message: 'Параметр "from" обязательный.'})
  @IsISO8601({ strict: true }, { message: 'Некорректная дата "from"' })
  from: string;

  @IsDefined({message: 'Параметр "to" обязательный.'})
  @IsISO8601({ strict: true }, { message: 'Некорректная дата "to"' })
  to: string;
}
