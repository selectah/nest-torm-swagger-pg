import { IsDefined, IsNumber, validateSync } from 'class-validator';
import { plainToClass } from 'class-transformer';

class EnvironmentVariables {
  @IsDefined()
  API_HOST: string;

  @IsDefined()
  @IsNumber()
  API_PORT: number;

  @IsDefined()
  DATABASE_URL: string;

  @IsDefined()
  @IsNumber()
  ROOMS_QUANTITY: number;
}

export function validateConfig(config: Record<string, unknown>) {
  const validatedConfig = plainToClass(
    EnvironmentVariables,
    config,
    { enableImplicitConversion: true },
  );
  const errors = validateSync(validatedConfig, { skipMissingProperties: false });

  if (errors.length > 0) {
    throw new Error(errors.toString());
  }
  return validatedConfig;
}
